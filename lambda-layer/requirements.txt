jsonpickle==1.3
aws-xray-sdk==2.4.3
boto3==1.14.38
elasticsearch==7.0.0
requests==2.7.0
requests-aws4auth==1.0
shortuuid==1.0.1